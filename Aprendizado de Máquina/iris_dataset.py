import mglearn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

# Carrega os dados
iris_dataset = load_iris()
# Os dados são organizados por 4 chaves: DESCR, target_names, data, feature_names e target
print('Chaves do dataset: \n{}'.format(iris_dataset.keys()))
# Uma descrição do conjunto de dados
print(iris_dataset['DESCR'][:193] + '\n...')
# Os nomes de cada coluna
print(iris_dataset['target_names'])
# Os dados em cada coluna
print(iris_dataset['data'])
# A classificação é representada por valors de 0 a 2
# sendo 0 setosa, 1 versicolor e 2 virginica
print('Alvos: \n{}'.format(iris_dataset['target']))

X_train, X_test, y_train, y_test = train_test_split(iris_dataset['data'], iris_dataset['target'], random_state=0)

iris_dataframe = pd.DataFrame(X_train, columns=iris_dataset.feature_names)
grr = pd.scatter_matrix(iris_dataframe, c=y_train, figsize=(15, 15), marker='o',
                        hist_kwds={'bins': 20}, s=60, alpha=.8, cmap=mglearn.cm3)
plt.show()

knn = KNeighborsClassifier(n_neighbors=1)
knn.fit(X_train, y_train)
X_new = np.array([[5, 2.9, 1, 0.2]])
prediction = knn.predict(X_new)
print("Prediction: {}".format(prediction))
print("Predicted target name: {}".format(iris_dataset['target_names'][prediction]))

y_pred = knn.predict(X_test)
print("Test set predictions:\n{}".format(y_pred))

print("Test set score: {:.2f}".format(np.mean(y_pred == y_test)))
