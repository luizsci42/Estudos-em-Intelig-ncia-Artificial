'''
Rede Neural para simular o operador OR
'''


import numpy as np

# Valores de entrada
entradas = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
# Saidas esperadas
saidas = np.array([0, 1, 1, 1])
# Pesos iniciais de cada entrada
pesos = [0, 0, 0, 0]
# A taxa pela qual a função treinar irá incrementar os pesos
taxaAprendizagem = 0.1


def step_function(soma):
    if soma >= 1:
        return True
    return False


def soma(inputs, weights):
    return inputs.dot(weights)

def treinar():
    print("A definir")

