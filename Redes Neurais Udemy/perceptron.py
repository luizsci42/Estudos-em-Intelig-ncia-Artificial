'''
Rede Neural Assistida para simular o operador AND
'''
import numpy as np

entradas = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
pesos = np.array([0.0, 0.0])
# Vetor com os valores esperados como saída
saidas = np.array([0, 0, 0, 1])
taxaAprendizagem = 0.1


# A Step Function é ativada apenas quando a entrada for >= 1
def step_function(soma):
    if soma >= 1:
        return 1
    return 0


def calcula_saida(registro):
    # Calcula o produto interno entre registro e pesos
    s = registro.dot(pesos)
    return step_function(s)


def treinar():
    erroTotal = 1
    while erroTotal != 0:
        erroTotal = 0
        for i in range(len(saidas)):
            saidaCalculada = calcula_saida(np.asarray(entradas[i]))
            erro = abs(saidas[i] - saidaCalculada)
            erroTotal += erro
            for j in range(len(pesos)):
                pesos[j] += taxaAprendizagem * entradas[i][j] * erro
                print("Peso atualizado: " + str(pesos[j]))
        print("Total de Erros:" + str(erroTotal))


treinar()
