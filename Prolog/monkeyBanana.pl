/* Problema do macaco e da banana: 

 Um estado é representado pelo termo:
 Estado(MonkeyHorizontal, MonkeyVertical, BoxPosition, HasBanana).
 move(State1, Move, State2): determinado movimento muda o programa do Estado1 para Estado2

 Há quatro coisas que o macaco pode fazer:
  1) Pegar a banana
  2) Subir na caixa
  3) Empurrar a caixa
  4) Caminhar
 Cada uma destas ações é representada pelas seguintes cláusulas:
*/

% Pegar a banana (Condição final / objetivo)
move(state(middle,onbox,middle,hasnot), grasp, state(middle,onbox,middle,has)).
% Subir na caixa
move(state(P,onfloor,P,H), climb, state(P,onbox,P,H)).
% Empurrar a caixa
move(state(P1,onfloor,P1,H), push(P1,P2), state(P2,onfloor,P2,H)).
% Caminhar
move(state(P1,onfloor,B,H), walk(P1,P2), state(P2,onfloor,B,H)).

% O programa finaliza em qualquer estado que o macaco tenha a banana
canget(state(_,_,_,has)).
/* Se o macaco pode se mover de um Estado1 para um Estado2
 e no Estado2, ele pode pegar a banana, então ele pode pegar
 no Estado1 */
canget(State1) :- move(State1,Move,State2), canget(State2).

% canget(state(atdoor, onfloor, atwindow, hasnot)).