s(Stacks,[Stack1, [Top1 | Stack2] | OtherStacks]) :-
	del([Top1 | Stack1], Stacks, Stacks1),
	del(Stack2, Stacks1, OtherStacks).

del(X, [X|L], L).
del(X, [Y | L], [Y|L1]) :-
	del(X, L, L1).

goal(Situation) :-
	member([a,b,c], Situation).

% Diz se determinado elemento pertence a uma lista 
member(_, []) :- fail. % Se a lista é vazia, falso 
member(X, [X|_]). % Se x é a cabeça da lista, verdadeiro
member(X, [_|Y]) :- member(X,Y). % Percorre a cauda recursivamente até encontrar o elemento

solve(Start, Solution).

% solve([[c,a,b],[],[]], Solution).