
/*
 Os passos necessários para finalizar a tarefa:
  1) O jogo finaliza quando não houver discos na Origem
  2) Mover N-1 discos da Origem para Auxiliar, utilizando Destino como temporário
  3) Mover apenas um disco de Origem para Destino
  4) Mover N-1 discos de Auxiliar para Destino, utilizando Origem como temporário

 O estado do jogo é reresentado pelo termo:
  hanoi(NumeroDiscos, Barra1, Barra2, Suporte3).
*/

% 1) O jogo finaliza quando não houver discos na Origem (Etapa Básica)
% Estado final (condição objetivo)
hanoi(0,_,_,_).

% Etapa recursiva:
% Estado espaço: quais as regras e quais os movimentos legais
% 3) Mover apenas um disco de Origem para Destino
hanoi(N,Origem,Auxiliar,Destino) :- 
	N1 is N-1,
	% 2) Mover N-1 discos da Origem para Auxiliar, utilizando Destino como temporário
	hanoi(N1,Origem,Destino,Auxiliar),
	write(['mover disco de',Origem,'para',Destino]),nl,
	% 4) Mover N-1 discos de Auxiliar para Destino, utilizando Origem como temporário
	hanoi(N1,Auxiliar,Origem,Destino).

% Estado inicial
/* hanoi(3, a, b, c). */