sem_ataque(_+_,[]).

sem_ataque(X+Y, [X1+Y1 | Cauda]) :-
X =\= X1,
Y =\= Y1,
Y-X =\= Y1-X1,
X+Y =\= X1+Y1,
sem_ataque(X+Y, Cauda).

solucao(_,[]).
solucao(N,[X+Y | Cauda]) :-
  solucao(N,Cauda),
  range(1,N,A),
  member(Y, A),
  sem_ataque(X+Y, Cauda).

member(_,[]) :- fail.
member(X, [X|_]).
member(X, [_|Cauda]) :- member(X,Cauda).

range(N, N, [N]) :- !.
range(N, M, [N|Cauda]) :-
        N1 is N + 1,
        range(N1, M, Cauda).