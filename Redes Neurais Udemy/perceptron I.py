import numpy as np

# np.array cria um vetor numpy
entradas = np.array([1, 7, 5])
pesos = np.array([0.8, 0.1, 0])


def soma(inputs, weights):
    # Calucla o produto escalar
    return inputs.dot(weights)


def step_function(valor_soma):
    if valor_soma >= 1:
        return True
    return False


valorFinal = soma(entradas, pesos)
final = step_function(valorFinal)

# Indica se o neurônio será ativado (True) ou não (False)
print(final)
