'''
Exemplo de rede neural multicamada utilizando a função sigmoide

Função sigmoide: y = 1 / 1 + e^(-x)
'''

import numpy as np


def sigmoide(soma):
    return 1 / 1 + np.exp(-soma)


entradas = np.array([0, 0], [0, 1], [1, 0], [1, 1])
saida = np.array([0, 1, 1, 0])
# Pesos para o x1 e para o x2
pesos0 = np.array([[-0.424, -0.740, -0.961], [-0.358, -0.577, -0.469]])
# Pesos da camada oculta para a camada de saída
pesos1 = np.array([[-0.017], [-0.893], [0.148]])
trainingTime = 100

for j in range(trainingTime):
    camadaEntrada = entradas
    somaSinapse0 = np.dot(camadaEntrada, pesos0)
    camadaOculta = sigmoide(somaSinapse0)
